//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

  var bodyParser = require('body-parser');
  app.use(bodyParser.json());
  app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
    next();
  });

var requestjson = require('request-json');

var path = require('path');

var urlMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3mb79587/collections/movimientos/?apiKey=krrpcDDp42z0DaTCN__IuLf8LgnAcYRO"

var clienteMLab = requestjson.createClient(urlMovimientos);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
/*
//REGRESA UN JSON CON 3 CLIENTES FICTICIOS
app.get('/',function (req, res){//peticion de tipo app get
  //res.send('Hola tu, node');
  res.sendFile(path.join(__dirname,'index.html')); //envio de hipermedia
});

app.post('/',function (req, res){//peticion de tipo app post
  res.send('Su peticion ha sido recibida');
});

//REGRESA UN JSON CON 3 CLIENTES FICTICIOS ID y NOMBRE
app.get('/',function (req, res){//peticion de tipo app get

  res.sendFile(path.join(__dirname,'personas.json')); //envio de hipermedia
});*/

//para pasar solo el id de algun objetos JSON
//:idcliente es una variable donde va a caer el id

app.get('/clientes/:idcliente', function (req, res){
  res.send('Aqui tiene el cliente  numero:'+ req.params.idcliente);
});

//app.post('/',function (req, res){//peticion de tipo app post
  //res.send('Su peticion POST ha sido recibida');
//});

app.put('/',function (req, res){//peticion de tipo  put
  res.send('Su peticion PUT ha sido recibida');
});

app.delete('/',function (req, res){//peticion de tipo delete
  res.send('Su peticion DELETE ha sido recibida');
});

app.post('/',function (req, res){//peticion de tipo app post
  res.send('Su peticion POST ha sido recibida cambiada');
});

app.get('/movimientos', function (req, res){
  clienteMLab.get('',function(err, resM, body){
    if(err){console.log(err);
    } else {res.send(body);
    }
    });
});

app.post('/movimientos', function (req, res){
  clienteMLab.post('',req.body, function(err, resM, body){
    if(err){console.log(err);
    } else {res.send(body);
    }
    });
});
